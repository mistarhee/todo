#! /bin/bash

cp *todo /usr/local/bin

echo 'todoLines=`wc -l ~/.todo | cut -d " " -f 1`' >> ~/.bashrc
echo 'echo "Welcome $USER"' >> ~/.bashrc
echo 'echo "You have $todoLines todos"' >> ~/.bashrc
